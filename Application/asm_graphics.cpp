#include "inc/asm_graphics.h"

using namespace ASM;

void Graphics::ASM_clearDisplay(){
	Display::clearDisplay();
}

void Graphics::ASM_drawPixel(int8_t xPos, int8_t yPos, int8_t color){
    if (xPos >= 0 && yPos >= 0 && xPos < 32 && yPos < 16) {
        Display::drawPixel(xPos, yPos, color);
    }
}

void Graphics::ASM_drawText(char* text, int16_t x, int16_t y, int16_t color, int16_t scaleFactor){
	Display::drawText(text, x, y, color, scaleFactor);
}

/*
void Graphics::ASM_drawDemo1(){
	int xPos = left.xPos;
	int yPos = left.yPos;

        for(int i=0; i<left.width; i++){
		for(int k=0; k<left.height; k++){
			if(left.ent.matrix[i][k] == 1){
				ASM_drawPixel(i+xPos, k+yPos, left.color);
			}
		}
	}
}
*/

void Graphics::ASM_drawDemo2(){
	    Display::clearDisplay(); 
	    Display::drawText("demo", 0, 0, ASM_GREEN, 0);
    Display::refresh();
      /*
        uint8_t bitmap[] = {1,0,0,1,1,0,1,1,1,1,1,1,1,1,0,1,0,0};
	display.drawBitmap(8,5, bitmap, 3, 6, ASM_GREEN);
	*/

}
