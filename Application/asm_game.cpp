#include "inc/asm_game.h"
#include "inc/asm_view.h"
#include "inc/asm_player.h"
// #include <display.h>
// #include <numericDisplay.h>
//#include <sine.h>
//#include "Sound.h"

using namespace ASM;

// weitere Todo's:
// - Einige offt verwendete Konstanten definieren
// - aktuelle patternlänge als Attribut speichern nicht hardcoden
// - pattern tatsächlich aufnehmen
// - aufgenommenes pattern mit generiertem vergleichen
// - code auslagern

Asm::Asm(Joystick & leftJoystick, Joystick & rightJoystick) :
Game (leftJoystick,rightJoystick,"ASM"){}

void Asm::prepareDemo(void){	// runs once
  Serial.println("Preparing the demo...");
	Display::clearDisplay();
  // demo gruppe
}
void Asm::playDemo(void){		// runs in loop till START_BUTTON_PRESSED
  Serial.println("Playing the demo...");
	int cycle = this->time % 2000;
	if (cycle < 1500 && demoState != 0) {
	      Display::clearDisplay();
        testScreen();
        sg.generate(1, false);
        
        showInput(sg.getSequence(0), Display::getColor(7,7,7));
        demoState = 0;
    } else if (cycle > 1500 && demoState != 1) {
	      Display::clearDisplay();
        testScreen();
        demoState = 1;
    }

  /*
  Display::clearDisplay();
  Display::drawText("demo...", 0, 0, ASM_GREEN, 0);
  Display::refresh();
  */
}

// void Asm::configMultiplayerGame(void) {

//   (StartButton::getStatus() == START_BUTTON_PRESSED)
//     uint16_t nameTextColor;
//     nameTextColor = Display::getColor(0, 2, 0);

//     Display::clearDisplay();
//     Display::drawText(this->name, 4, 0, nameTextColor, 1);


//     Display::refresh();

// }

void Asm::configureGame(void){	// runs in loop till START_BUTTON_PRESSED
	Serial.println("Configuring the game...");
  // demo gruppe

  // test values

  /*this->gameMode = PLAYER_MODE_MULTIPLAYER ;
     this->difficulty = DIFFICULTY_MODE_MEDIUM ;
  this->difficulty = DIFFICULTY_MODE_MEDIUM ;*/
  this->subState = GAME_SUBSTATE_INIT;
}



void Asm::process(void){		// runs in loop forever
	// TODO !! this needs to catch GAME_STATE_CONFIG since we want to have 2 screens for config
 Game::process();
}

void Asm::prepareGame(void){	// runs once
	Serial.println("Preparing the game...");
	// WIP
	// init players etc
  // TODO dont mark j2 active if singleplayer
  this->p1 = new Player(this->joystickLeft, true);
  this->p2 = new Player(this->joystickRight, true);
  win = true;
	// save time
	this->timeStart();
	// game loop starting
  this->subState = GAME_SUBSTATE_INIT;
  // tmp: for dot game
  this->dotGameX = 0;
	this->dotGameY = 0;
}

void Asm::drawDot(void){
  Display::clearDisplay();
  Display::drawPixel(this->dotGameX, this->dotGameY, ASM_GREEN);
  Display::refresh();
}

void Asm::dotEvaluate(Joystick* j1, Joystick* j2){
  if(j2->isUp() && this->dotGameY > 0) {
    this->dotGameY = (this->dotGameY - 1) % 16;
  }
  if(j2->isDown()){
    this->dotGameY = (this->dotGameY + 1) % 16;
  }
  if(j2->isLeft() && this->dotGameX > 0){
    this->dotGameX = (this->dotGameX - 1) % 32;
  }
  if(j2->isRight()){
    this->dotGameX = (this->dotGameX + 1) % 32;
  }
}

void Asm::dotGame(void){
	drawDot();
	dotEvaluate(this->joystickLeft, this->joystickRight);
}

void Asm::echoJoystick(void){
	Joystick* j1 = this->joystickLeft;
	Joystick* j2 = this->joystickRight;

	if (j1->isLeft() || j1->isUp() || j1->isRight() || j1->isDown()){
	  	char buff[200];
	    sprintf(buff, "Left Joystick : Left: %d Up: %d Right: %d Down: %d",
                          j1->isLeft(), j1->isUp(), j1->isRight(), j1->isDown());
	    Serial.println(buff);
  	}
  	if (j2->isLeft() || j2->isUp() || j2->isRight() || j2->isDown()){
	  	char buff2[200];
	    sprintf(buff2, "Right Joystick : Left: %d Up: %d Right: %d Down: %d",
                          j2->isLeft(), j2->isUp(), j2->isRight(), j2->isDown());
	    Serial.println(buff2);
  	}
}

bool Asm::asmAnimationReady(void){
	  int msPassed = this->time - this->timeCursor;
   	int step = 1000;
   	int offset_x = 0;
   	int offset_y = 7;
   	uint16_t color = 0;

    char buff2[200];
    sprintf(buff2, "MS passed : %d | time: %d | loopStartTime : %d", msPassed, this->time, this->loopStartTime);
    Serial.println(buff2);
	if (msPassed < (step * 1)){
		Serial.println("3");
		color = Display::getColor(7, 0, 0);
		offset_x = 7;
	} else if (msPassed < (step * 2)) {
		Serial.println("2");
		color = Display::getColor(7, 7, 0);
		offset_x = 14;
	} else if (msPassed < (step * 3)) {
		Serial.println("1");
		color = Display::getColor(0, 0, 7);
		offset_x = 21;
	} else if (msPassed < (step * 4)){
		Serial.println("Learn :");
    Display::clearDisplay();
    Display::drawText("learn", 0, 0, ASM_GREEN, 0);
    Display::refresh();
    return true;
	} else {
		return false;
	}

	Display::clearDisplay();

	Display::drawPixel(offset_x + 0, offset_y + 0, color);
	Display::drawPixel(offset_x + 0, offset_y + 1, color);
	Display::drawPixel(offset_x + 1, offset_y + 0, color);
	Display::drawPixel(offset_x + 1, offset_y + 1, color);

	Display::refresh();
	return true;
}

bool Asm::asmAnimationPattern(void){
  // Animieren des generierten Pattern
  int duration = 5;
  int step = 1000;
  int msPassed = this->time - this->timeCursor;
	Display::clearDisplay();
  uint16_t show_color = Display::getColor(7, 7, 0);

  // Nach "Patternlänge" * 1000 ms ist diese State fertig
  if (msPassed < duration * step) {
    ASM::showInput(sg.getSequence(msPassed/step), show_color);
    return true;
  }
	return false;
}

bool Asm::asmAnimationRepeat(void){
  // Zeige ready für eine Sekunde an
  Display::clearDisplay();
  int msPassed = this->time - this->timeCursor;
   	int step = 1000;

    char buff2[200];
    sprintf(buff2, "MS passed : %d | time: %d | loopStartTime : %d", msPassed, this->time, this->loopStartTime);
    Serial.println(buff2);
	if (msPassed < (step * 1)) {
		Serial.println("repeat !");
    Display::drawText("repeat!", 0, 0, Display::getColor(7, 0, 0), 0);
	} else {
		return false;
	}
	Display::refresh();
	return true;
}

char Asm::getInput(Joystick *j) {
  /*char buff2[200];
  sprintf(buff2, "Right Joystick : Left: %d Up: %d Right: %d Down: %d", j->isLeft(), j->isUp(), j->isRight(), j->isDown());
  Serial.println(buff2);*/

  Input ret = -1;
  if (j->isUp()) {
    ret = Input::Up;
  } else if (j->isLeft()) {
    ret = Input::Left;
  } else if (j->isRight()) {
    ret = Input::Right;
  } else if (j->isDown()) {
    ret = Input::Down;
  } else if (j->isButtonTop()) {
    ret = Input::Button;
  }

  return ret;
}

int Asm::asmEvaluate(void){ // runs in loop
  char input = p2->getJoystickState();
  int ret = 0;

  uint16_t color = Display::getColor(7,7,7);
  Display::clearDisplay();
  if ( input >= 0 && acceptNextInput) {
    if(input != lastInput || acceptNextInput) {
      if (p2->getCurrentPatternLocation() < 5) {
        lastInput = input;
        acceptNextInput = false;
        this->timeCursor = this->time;
        if (sg.validateSequence(lastInput, p2->getCurrentPatternLocation()) {
          p2->advanceInPattern();
        } else {
          /*color = Display::getColor(7,0,0);
          p2->advanceInPattern();*/
          win = false;
          ret = -1;
        }
      } else {
        ret = -1;
      }
    }
    uint16_t right_color = ASM_GREEN;
    showInput(lastInput, color);
    
    }else {
      if ((this->time - this->timeCursor) > 100) {
        acceptNextInput = true;
      }
    }


    Display::refresh();



	// tmp code for testing :
    if (this->joystickLeft->isButtonBody())
	   {
		     return -1;
       }
    if (Asm::canSkip()){
    	return 1;
    }

	return ret;
}

bool Asm::canSkip(void){
  uint32_t skip_lock = 500;
  if((StartButton::getStatus() == START_BUTTON_PRESSED) && ((this->time - this->skipCursor) > skip_lock)){
    return true;
  }
  return false;
}


void Asm::asmGame(void){ // runs in loop
  // Jeder State kann aktuell beim Drücken eines Joystick Buttons übersprungen werden
  // Sollte später geändert werden
  switch (this->subState) {
		case GAME_SUBSTATE_INIT:
      // Initialisiere game variablen
      // todo auslagern
			Serial.println("GAME_SUBSTATE_INIT");
			this->loopStartTime = this->time;
			this->subState = GAME_SUBSTATE_ANIMATION_READY;
      this->timeCursor = this->time;
      acceptNextInput = false;
      // Nur zu Testzwecken
      // Achtung! Patterngröße noch hardcoded überall
      sg.generate(5, true); // either this has to be true or the pattern animation needs to be rewritten to blank between repeating input
      Serial.println("pattern generated");
      this->skipCursor = this->time;
      Serial.println("skipCursor reset");
			break;
		case GAME_SUBSTATE_ANIMATION_READY:
      // Zeigt 3 Sekunden Animation und dann Ready an
    	Serial.println("GAME_SUBSTATE_ANIMATION_READY");
    	if(!asmAnimationReady() || Asm::canSkip()){
        this->subState = GAME_SUBSTATE_ANIMATION_PATTERN;
        this->timeCursor = this->time;
        this->skipCursor = this->time;
      }
      break;
    case GAME_SUBSTATE_ANIMATION_PATTERN:
      // Gibt das Pattern auf dem Display an
      Serial.println("GAME_SUBSTATE_ANIMATION_PATTERN");
      if(!asmAnimationPattern() || Asm::canSkip()){
        this->subState = GAME_SUBSTATE_ANIMATION_REPEAT;
        this->timeCursor = this->time;
        this->skipCursor = this->time;
      }
      break;
    case GAME_SUBSTATE_ANIMATION_REPEAT:
        // Zeigt eine Sekunde lang repeat auf dem Display
      	Serial.println("GAME_SUBSTATE_ANIMATION_REPEAT");
        if(!asmAnimationRepeat() || Asm::canSkip()){
          this->subState = GAME_SUBSTATE_EVALUATE;
          this->timeCursor = this->time;
          this->skipCursor = this->time;
        }
        break;
    case GAME_SUBSTATE_EVALUATE:
    	switch (asmEvaluate()) {
        // Hier spielt sich die Gamelogik ab, aktuell wird nur die aktuelle Richtung des rechten
        // Joystick auf dem Display angezeigt
    		case 0: // keep loopin'
    			break;
    		case 1: // restart
    			this->subState = GAME_SUBSTATE_INIT;
    			break;
    		case -1:
          this->state = GAME_STATE_END;  // this runs gameOver automatically
    			break; // end
    	}
    }
}

void Asm::playGame(void)
{
	// dotGame();
	// echoJoystick();
	asmGame();
}

void Asm::gameOver(void)
{
	Serial.println("Game Over !");
  Display::clearDisplay();
  if (win) {
     Display::drawText("Win", 0, 0, Display::getColor(0,7,0), 0);
  } else {
    Display::drawText("End", 0, 0, Display::getColor(7,0,0), 0);
  }
  Display::refresh();

  // WIP
}

/*void play(void)
{
  // WIP
}

void Asm::draw(void)
{
  // WIP
}*/
