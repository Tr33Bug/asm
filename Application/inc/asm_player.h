#ifndef ASM_PLAYER_H_
#define ASM_PLAYER_H_

#include "Joystick.h"

#define NUM_SINGLEPLAYER_LIVES 3

namespace ASM{
  class Player {
      private:
          Joystick* stick;
          //current score
          uint32_t score;
          //save the location in the current pattern
          uint8_t currentPatternLocation;
          //Remaining lives
          uint8_t lives;
          bool failed = false;
          bool isActive;

      public:
          Player(Joystick* a, bool isPlayerActive);

          uint8_t getJoystickState();

          //returns state of the confirm button
          bool getButtonState();

          // reset all values to default at the beginning of the game
          void reset();

          // reset pattern location in between rounds
          void resetRound();

          // increase score
          void increaseScore(uint32_t extraScore);

          // reduce score by 1
          // Diese Funktion wird nur im Multiplayer
          // verwendet falls keiner der Spieler seine Eingabe bestätigt
          void decreaseScore();

          void fail();

          bool getFailureState();

          void advanceInPattern();

          uint8_t getCurrentPatternLocation();

          uint8_t getLives();

          uint32_t getScore();
      };
}
#endif
