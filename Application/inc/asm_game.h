#ifndef ASM_H_
#define ASM_H_

#include "game.h"
#include "Joystick.h"
#include "asm_graphics.h"
#include "SequenceGenerator.h"
#include "asm_player.h"

#define PLAYER_MODE_SINGLEPLAYER	0
#define PLAYER_MODE_MULTIPLAYER		1
#define DIFFICULTY_MODE_EASY		0
#define DIFFICULTY_MODE_MEDIUM		1
#define DIFFICULTY_MODE_HARD		2


enum Substate {	GAME_SUBSTATE_CONFIG,
				GAME_SUBSTATE_INIT,
				GAME_SUBSTATE_ANIMATION_READY,
				GAME_SUBSTATE_ANIMATION_PATTERN,
				GAME_SUBSTATE_ANIMATION_REPEAT,
				GAME_SUBSTATE_EVALUATE
			};


namespace ASM {
	class Asm : public Game
	{
	private:
		// GameStatus* gameStatus;
		/*uint8_t selectMode(Joystick *JoystickLeft, Joystick *JoystickRight);
		uint8_t selectDifficulty(Joystick *JoystickLeft, Joystick *JoystickRight);*/

		Graphics graphics;
		Substate subState;
		int demoState = 0;

		uint32_t loopStartTime;
		uint32_t timeCursor;
		uint16_t patternCursor;
		uint32_t skipCursor;

		SequenceGenerator sg;
		Player* p1;
		Player* p2;

		// temp
		int dotGameX;
		int dotGameY;
		Input lastInput;
		bool acceptNextInput;
		bool win;

	public:
		Asm(Joystick & leftJoystick,Joystick & rightJoystick);

		void prepareDemo(void);
		void playDemo(void);

		//void configMultiplayerGame(void);
		void configureGame(void);

		void prepareGame(void);
		void playGame(void);

	  	void gameOver(void);
		void process(void);

		//rsd
		bool asmAnimationReady(void);
		bool asmAnimationPattern(void);
		bool asmAnimationRepeat(void);

		int asmEvaluate(void);
		void asmGame(void);

		char getInput(Joystick *j);
		bool canSkip(void);

		// temp
		void drawDot(void);
		void dotEvaluate(Joystick* j1, Joystick* j2);
		void dotGame(void);
		void echoJoystick(void);
	};
}

#endif
