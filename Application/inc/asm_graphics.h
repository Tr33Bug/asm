#ifndef ASM_GRAPHICS_H_
#define ASM_GRAPHICS_H_

#include "display.h"
#include <stdint.h>
//#include <string>


#define ASM_GREEN Display::getColor(0, 7, 0)
/*#define BLUE display->getColor(0, 0, 7)
#define RED display->getColor(7, 0, 0)
#define CLEAR display->getColor(0, 0, 0)
#define YELLOW display->getColor(7, 7, 0)
#define WHITE display->getColor(7, 7, 7)*/

/*struct entity{
	int matrix[][];
	int8_t color;
} entity;

struct left{
	struct entity ent;
	int8_t xPos;
	int8_t yPos;
	int width;
	int height;
} left = {
		{
				{{0,0,1},
				{0,1,1},
				{1,1,1},
				{1,1,1},
				{0,1,1},
				{0,0,1}},
				GREEN
		},
		8, //xPos
		5,  //yPos
		3, //width
		6 //height
};

*/
namespace ASM{

	class Graphics{
	private:
		Display display;
	public:
		void ASM_drawText(char* text, int16_t x, int16_t y, int16_t color, int16_t scaleFactor);
		void ASM_drawPixel(int8_t xPos, int8_t yPos, int8_t color);
		void ASM_clearDisplay();
		//void ASM_drawDemo1();
		void ASM_drawDemo2(); // Mit drawBitmap
	};
}


#endif
