#ifndef ASM_VIEW_H_
#define ASM_VIEW_H_

#include <stdint.h>
#include "display.h"
#include "SequenceGenerator.h"

namespace ASM{
	void drawMatrix(uint16_t matrix[16][32], uint16_t height, uint16_t width, uint16_t background);
	void testScreen(void);
	void addObject(uint16_t matrix[16][32], uint16_t obj[16][32], uint16_t offset_x, uint16_t offset_y, uint16_t width, uint16_t height);
	void blankMatrix(uint16_t matrix[16][32], uint16_t b);
	void testScreen2(void);

	void showLeft(uint16_t color);
	void showUp(uint16_t color);
	void showRight(uint16_t color);
	void showDown(uint16_t color);
	void showCenter(uint16_t color);
	void showInput(Input inp, uint16_t color);
}

#endif
