/**
 * Author: Henning Oehmen und Josef Grieb
 * Datum: 13.06.2017
 * Automatisierungslabor - Softwaregruppe
 */

#ifndef SPIELELOGIK_CONTROLLER_H
#define SPIELELOGIK_CONTROLLER_H

#include "Arduino.h"

#include "Microcontroller.h"
#include "display.h"
#include "CoinDetection.h"
#include "Joystick.h"



#include <Wire.h>


// # Game variables #
unsigned int totalCoinValue;
unsigned int currentCoinValue;


// # Game objects #

/*
Ball* ball;
Player* player1;
Player* player2;
*/





Display* display;


void setup();
void loop();

void ca_coinDetected(CoinDetection::coin coinType);
void checkCoinValues();

void checkScoreAndPlaySound();


#endif //SPIELELOGIK_CONTROLLER_H
