#ifndef COINDETECTION_H_
#define COINDETECTION_H_

#include "Arduino.h"

class CoinDetection {
    static bool coinToggle;          // to check if a coin signal appear. - merke den zustand der letzten flanke
    static uint8_t timer;        // to save the time how long the coin signal toggles
    const volatile uint8_t pulse1 = 4;          // the falnk nr for coin1 (coin 1 = 1 pulse = 2 Fanks = 1x high + 1x low flank)
    const volatile uint8_t pulse2 = 6;         // ... for coin 2
    const volatile uint8_t pulse3 = 8;         // ... place holder for future coins
    uint8_t flankenCounter;         // count the number of flanks. 2 Flanks = 1 pulse


  public:
	volatile uint8_t pinLED;
    CoinDetection();
	volatile uint8_t pinCoinAcceptor;
    enum coin {
      cent5,
      cent10,
      cent20, 
      cent0
    };
    void cd_returnCoin();
    void cd_coinDetection();
    
  
  
};

#endif