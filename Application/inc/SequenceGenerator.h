#ifndef ASM_SEQUENCEGENERATOR_H
#define ASM_SEQUENCEGENERATOR_H

#include <stdint.h>
#include <string.h>
#include <Arduino.h>

enum Input : uint8_t
{
    Button,
    Up,
    Down,
    Left,
    Right
};

class SequenceGenerator
{
private:
    const uint8_t MAX_SEQUENCE_SIZE = 16;
    uint8_t CurrentSequenceSize = 0;
    Input* Sequence;
public:
    SequenceGenerator();
    Input* getSequence();
    Input getSequence(uint8_t index);
    void generate(uint8_t size, bool distinctPrevious);
    bool validateSequence(Input input, uint8_t iteration);
    ~SequenceGenerator();
};

#endif