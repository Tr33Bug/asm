#include "inc/asm_player.h"

using namespace ASM;

// Constructor
// creates a player instance for a given Joystick
Player::Player(Joystick* a, bool isPlayerActive) {
    stick = a;
    isActive = isPlayerActive;
    this->reset();
}


uint8_t Player::getJoystickState() {
    if(stick->isButtonTop()){
        return 0;
    } else if(stick->isUp()){
        return 1;
    } else if(stick->isDown()){
        return 2;
    } else if(stick->isLeft()){
        return 3;
    } else if(stick->isRight()){
        return 4;
    }
}

//returns state of the confirm button
bool Player::getButtonState() {
    return stick->isButtonTop();
}

// reset all values to default at the beginning of the game
void Player::reset() {
    currentPatternLocation = 0;
    failed = false;
    lives = NUM_SINGLEPLAYER_LIVES;
    score = 0;
}

// reset pattern location in between rounds
void Player::resetRound() {
    currentPatternLocation = 0;
    failed = false;
}

// increase score
void Player::increaseScore(uint32_t extraScore) {
    score = score + extraScore;
}

// reduce score by 1
// Diese Funktion wird nur im Multiplayer
// verwendet falls keiner der Spieler seine Eingabe bestätigt
void Player::decreaseScore() {
    if(score > 0) {
        score--;
    }
}

void Player::fail() {
    failed = true;
    lives--;
}

bool Player::getFailureState() {
    return failed;
}

void Player::advanceInPattern() {
    currentPatternLocation++;
}

uint8_t Player::getCurrentPatternLocation() {
    return currentPatternLocation;
}

uint8_t Player::getLives() {
    return lives;
}

uint32_t Player::getScore() {
    return score;
}
