#include "inc/asm_view.h"

using namespace ASM;

void ASM::blankMatrix(uint16_t matrix[16][32], uint16_t b){
	uint16_t tmp[16][32] = {
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
		{b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b, b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b}
	};
	matrix = tmp;
}

void ASM::drawMatrix(uint16_t matrix[16][32], uint16_t height, uint16_t width, uint16_t background=Display::getColor(0, 0, 0)){
	for (int y = 0; y < height; y++) {
	    for (int x = 0; x < width; x++) {
	    	if (matrix[y][x] != background){
	    		Display::drawPixel(x, y, matrix[y][x]);
	    	}
	    }
	}
	Display::refresh();
}


void ASM::addObject(uint16_t matrix[16][32],
					uint16_t obj[16][32],
					uint16_t offset_x, uint16_t offset_y,
					uint16_t width, uint16_t height
					){
	for( int y=0; y < height; y++) {
        for( int x=0; x < width; x++) {
        	uint16_t tr_x = offset_x + x;
        	uint16_t tr_y = offset_y + y;
        	if (obj[y][x]){
        		matrix[tr_y][tr_x] = obj[y][x];
        	}
        }
    }
}


void ASM::testScreen2(void){
	// broken background has random colors
	Display::clearDisplay();
	uint16_t N = Display::getColor(7, 7, 7); // Background
	uint16_t U = Display::getColor(7, 0, 0); // up     arrow

	uint16_t matrix[16][32];
	ASM::blankMatrix(matrix, N);
/*
	uint16_t obj[16][32] = {
		{N,N,U, U,N,N},
		{N,U,U, U,U,N},
		{U,U,U, U,U,U}
	};

	ASM::addObject(matrix, obj, 5, 5, 6, 3);
	*/

	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::testScreen(void){
	//works
	uint16_t N = Display::getColor(0, 0, 0); // Background

	uint16_t C = Display::getColor(7, 0, 7); // center square

	uint16_t U = Display::getColor(7, 0, 0); // up     arrow
	uint16_t D = Display::getColor(7, 7, 0); // down   arrow
	uint16_t L = Display::getColor(0, 7, 7); // left   arrow
	uint16_t R = Display::getColor(0, 0, 7); // right  arrow

	uint16_t matrix[16][32] = {
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,U, U,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,U,U, U,U,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,U,U,U, U,U,U,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,L,N,N,N, N,N,N,R,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,L,L,N,N,N, N,N,N,R,R,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,L,L,L,N,N,C, C,N,N,R,R,R,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,L,L,L,N,N,C, C,N,N,R,R,R,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,L,L,N,N,N, N,N,N,R,R,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,L,N,N,N, N,N,N,R,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,D,D,D, D,D,D,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,D,D, D,D,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,D, D,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N}
	};
	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::showLeft(uint16_t color){
	uint16_t N = Display::getColor(0, 0, 0);
	uint16_t L = color;
	uint16_t matrix[16][32] = {
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,L,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,L,L,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,L,L,L,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,L,L,L,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,L,L,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,L,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N}
	};
	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::showUp(uint16_t color){
	uint16_t N = Display::getColor(0, 0, 0);
	uint16_t U = color;
	uint16_t matrix[16][32] = {
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,U, U,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,U,U, U,U,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,U,U,U, U,U,U,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N}
	};
	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::showRight(uint16_t color){
	uint16_t N = Display::getColor(0, 0, 0);
	uint16_t R = color;
	uint16_t matrix[16][32] = {
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,R,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,R,R,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,R,R,R,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,R,R,R,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,R,R,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,R,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N}
	};
	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::showDown(uint16_t color){
	uint16_t N = Display::getColor(0, 0, 0);
	uint16_t D = color;
	uint16_t matrix[16][32] = {
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,D,D,D, D,D,D,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,D,D, D,D,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,D, D,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N}
	};
	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::showCenter(uint16_t color){
	uint16_t N = Display::getColor(0, 0, 0);
	uint16_t C = color;
	uint16_t matrix[16][32] = {
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,C, C,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,C, C,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N},
		{N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N, N,N,N,N,N,N,N,N,N,N,N,N,N,N,N,N}
	};
	ASM::drawMatrix(matrix, 16, 32, N);
}

void ASM::showInput(Input inp, uint16_t color){
	switch(inp) {
		case Input::Up :
			Serial.println("Up");
			ASM::showUp(color);
			break;
		case Input::Down :
			Serial.println("Down");
			ASM::showDown(color);
			break;
		case Input::Left :
			Serial.println("Left");
			ASM::showLeft(color);
			break;
		case Input::Right :
			Serial.println("Right");
			ASM::showRight(color);
			break;
		case Input::Button :
			Serial.println("Button");
			ASM::showCenter(color);
			break;
	}
}
