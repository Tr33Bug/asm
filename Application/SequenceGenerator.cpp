#include "inc/SequenceGenerator.h"

SequenceGenerator::SequenceGenerator()
{
}

Input *SequenceGenerator::getSequence()
{
    return Sequence;
}

Input SequenceGenerator::getSequence(uint8_t index)
{
    return Sequence[index];
}

void SequenceGenerator::generate(uint8_t size, bool distinctPrevious)
{
    CurrentSequenceSize = size > SequenceGenerator::MAX_SEQUENCE_SIZE ? SequenceGenerator::MAX_SEQUENCE_SIZE : size;
    if (Sequence != NULL) {
        delete[] Sequence;
    }
    Sequence = new Input[size];
    Sequence[0] = (Input)random(0, 4);
    for (size_t i = 1; i < size; i++)
    {
        Input randomInput = (Input)random(0, 4);
        if (distinctPrevious)
        {
            while (randomInput == Sequence[i - 1])
            {
                randomInput = (Input)random(0, 4);
            }
        }
        Sequence[i] = randomInput;
    }
}

bool SequenceGenerator::validateSequence(Input input, uint8_t iteration)
{
    if (iteration >= CurrentSequenceSize || input != Sequence[iteration])
    {
        return false;
    }
    return true;
}

SequenceGenerator::~SequenceGenerator()
{
    delete[] Sequence;
}