#include <Wire.h>

/**
 * Author: Henning Oehmen und Josef Grieb
 * Datum: 13.06.2017
 * Automatisierungslabor - Softwaregruppe
 */
#include <Arduino.h>
#include "inc/Main.h"





#include <console.h>
/*#include "inc/pong_game.h"
#include "inc/inv_game.h"
#include "inc/curve_game.h"
#include "inc/sw_game.h"*/
#include "inc/asm_game.h"
#include "Midi.h"

Console console;
// upload test
void setup() {


    Serial.begin(115200);
    Midi::init();
    console.init();

    Game * game;
    /*game = new Pong(console.getJoystick(0), console.getJoystick(1));
    console.addGame(game);

    game = new Invaders(console.getJoystick(0), console.getJoystick(1));
    console.addGame(game);

    game = new CURVE::Curve(console.getJoystick(0), console.getJoystick(1));
    console.addGame(game);

    game = new Space_Wars(console.getJoystick(0), console.getJoystick(1));
    console.addGame(game);*/
  
    game = new ASM::Asm(console.getJoystick(0), console.getJoystick(1));
    console.addGame(game);

}

void loop() {


  console.process();

}
